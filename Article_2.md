Les attributions individuelles de l'indemnité prévue à l'article 1er ci-dessus sont fixées par décision du ministre chargé de l'agriculture, compte tenu de la valeur et de l'activité de chacun des agents appelés à en bénéficier, dans les limites comprises entre 50 p. 100 et 200 p. 100 des montants moyens annuels définis au deuxième alinéa ci-dessous.

Les montants moyens annuels servant de base au calcul des crédits pour l'attribution de cette indemnité sont fixés par arrêté conjoint des ministres chargés de l'agriculture, de la fonction publique et du budget.
